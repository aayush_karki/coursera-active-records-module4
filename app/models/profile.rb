class Profile < ActiveRecord::Base
  belongs_to :user

  validate :name_validation, :malename_validation, :gender_validation

  default_scope {order :birth_year}


  def name_validation
  	if self.first_name.nil? && self.last_name.nil?   
  		errors.add(:last_name, "first name and last name both can't be nil")
  	end
  end

  def gender_validation
  	a =  self.gender=="male" || self.gender=="female"  
  	if a == false
  		errors.add(:gender, "Gender has to be male or female only")
  	end
  end


  def malename_validation
  	if self.gender=="male"  
  		if self.first_name == "Sue"  
  			errors.add(:first_name, "A male first name can' be Sue")
  		end
  	end
  end


  def self.get_all_profiles(min_year, max_year)
  		return Profile.where('birth_year BETWEEN ? AND ?', min_year, max_year)
  end

end
